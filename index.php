<?php
    require_once ('animal.php');
    require_once ('Frog.php');
    require_once ('Ape.php');

    $sheep = new Animal ("Shaun");
        echo "Name : " . $sheep->name . "<br>";
        echo "legs : " . $sheep->legs . "<br>";
        echo "cool blooded : " . $sheep->cool_blooded . "<br><br>";

    $kodok = new Frog ("buduk");
        echo "Name : " . $kodok->name . "<br>";
        echo "legs : " . $kodok->legs . "<br>";
        echo "cool blooded : " . $kodok->cool_blooded . "<br>";
        echo $kodok->jump() . "<br><br>";

    $sungokong = new Ape ("kera sakti");
        echo "Name : " . $sungokong->name . "<br>";
        echo "legs : " . $sungokong->legs . "<br>";
        echo "cool blooded : " . $sungokong->cool_blooded . "<br>";
        echo $sungokong->yell() . "<br><br>";
    


?>